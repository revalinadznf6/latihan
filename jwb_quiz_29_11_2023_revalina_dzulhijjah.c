#include <stdio.h>
#include <stdlib.h>

int main()
{
  
  int bb = 0;
  int ba = 10;

    int i=1, N=0;
    float Data=0.0, Rata=0.0, Total=0.0;

    printf("Banyaknya data : ");
    scanf("%d", &N);
    Total = 0;

    do {
    printf("Masukan Data ke-%d: ", i);
    scanf("%f", &Data);
    
    if(Data <= bb || Data >= ba) {
      printf("Data tidak valid! Harus antara %d sampai %d\n", bb, ba);
      break; 
    }
    Total += Data;
    i++;
  } while(i <= N);
  if(i <= N) {
    Rata = Total / (i-1); 
  }
  else {
    Rata = Total / N;  
  }
    printf("Banyaknya Data : %d\n", N);
    printf("Total Nilai Data : %.2f\n", Total);
    printf("Rata-rata nilai Data : %.2f\n", Rata);

  return 0;
}
