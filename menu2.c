#include <stdio.h>
#include <stdlib.h>

const double pi = 3.14;

int main()
{
  

    int pilih=0;
    float sisi=0.0, jari=0.0, tinggi=0.0;

    do {
        printf("<<< Menu >>>\n\n");
        printf("1. Menghitung Isi Kubus\n");
        printf("2. Menghitung Luas Lingkaran\n");
        printf("3. Menghitung Isi Silinder\n");
        printf("0. Keluar Program\n");
        printf("Pilih Nomor : ");
        scanf("%d", &pilih);

        switch (pilih) {
            case 1:
                printf("Panjang Sisi Kubus : ");
                scanf("%f", &sisi);
                printf("Isi Kubus : %.2f\n", sisi * sisi * sisi);
                break;

            case 2:
                printf("Jari-jari lingkaran : ");
                scanf("%f", &jari);
                printf("Luas Lingkaran : %.2f\n", pi * jari * jari);
                break;

            case 3:
                printf("Jari-jari lingkaran : ");
                scanf("%f", &jari);
                printf("Tinggi Silinder : ");
                scanf("%f", &tinggi);
                printf("Isi Silinder : %.2f\n", pi * jari * jari * tinggi);
                break;
        }
        
   } while (pilih != 0); 

  return 0;
}
